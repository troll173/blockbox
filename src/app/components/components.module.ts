import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScanningComponent } from './scanning/scanning.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { ConnectWifiComponent } from './connect-wifi/connect-wifi.component';



@NgModule({
  declarations: [ScanningComponent, PermissionsComponent, ConnectWifiComponent],
  imports: [
    CommonModule, 
  ],
  exports: [
    ScanningComponent,
    PermissionsComponent, 
    ConnectWifiComponent
  ],
})
export class ComponentsModule { }
