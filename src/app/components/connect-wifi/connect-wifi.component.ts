import { Component, OnInit } from '@angular/core';
import { WifiWizard2 } from '@ionic-native/wifi-wizard-2/ngx';
import { AlertController, ModalController } from '@ionic/angular';
import { connect } from 'node:http2';

@Component({
  selector: 'app-connect-wifi',
  templateUrl: './connect-wifi.component.html',
  styleUrls: ['./connect-wifi.component.scss'],
})
export class ConnectWifiComponent implements OnInit {

  private networks = []

  constructor(private wifiWizard: WifiWizard2, private modalController: ModalController, private alertController: AlertController) { }

  ngOnInit() {
    this.getNetworks();
  }


  getNetworks(){
    this.wifiWizard.scan()
    .then(networks => {
      console.log("networks", networks);
      this.networks = networks;
    })
  }


  async connectToWifi(network){

    if(network.capabilities.includes("PSK")){

      const alert = await this.alertController.create({
        header: 'Connect to '+ network.SSID,
        inputs: [
          {
            name: 'password',
            type: 'password',
            placeholder: 'Wifi Password'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: (data) => {
              console.log('Confirm Okay', data);
              this.confirmDetails(network, data.password);
            }
          }
        ]
      });

      await alert.present();
    }else{
      this.confirmDetails(network, null);
    }
  }


  confirmDetails(network, password){
    let wifi = {
      "SSID": network.SSID,
      "password": password,
      "BSSID": network.BSSID
    };
    console.log("Connect to "+ network.SSID,  wifi);
    this.modalController.dismiss(wifi);
    
  }

}
