import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  private clientId = environment.spotify.client_id;
  private clientSecret = environment.spotify.client_secret;
  private redirectUrl = environment.spotify.redirect_url;
  
  constructor(private http: HTTP) {

   }


   getUserInfo(token, callback){
    this.http.get('https://api.spotify.com/v1/me', {}, {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer "+token
    })
    .then(data => {
      if(data.status == 200){
        console.log(data.data); 
        callback(true, data.data);
      }else{
        callback(false, data.data);
      }
    })
    .catch(error => {
      console.log(error.error);
      callback(false, error);
    });
   }

  requestTokens(code, callback){
 
    this.http.post('https://accounts.spotify.com/api/token', {
      grant_type: "authorization_code",
      code: code,
      redirect_uri: "https://athena-34153.web.app/",
      client_id: this.clientId,
      client_secret: this.clientSecret
    },
    {})
    .then(data => {
      console.log(data.status);
      if(data.status == 200){
        callback(true, data.data);
      }else{
        callback(false, data);
      } 
    })
    .catch(error => {
      callback(false, error.error);
      console.log(error.error);
    });
  }

 

}
