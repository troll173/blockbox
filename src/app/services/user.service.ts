import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


export interface User {
  name: string,  
  uid: string,
  home: string,
  spotify: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  collectionName = 'users';

  constructor(
    private firestore: AngularFirestore
  ) { }


  createUser(user: User) {
    return this.firestore.collection(this.collectionName).add(user);
  }

  readUsers() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

  searchUserByUid(uid){
    return this.firestore.collection(this.collectionName,  ref => ref.where("uid", "==", uid)).valueChanges({idField: 'userDataId'});
  }

  findUser(userID){
    return this.firestore.doc(this.collectionName + '/' + userID).valueChanges();
  }

  updateUser(userID, user: User) {
    return this.firestore.doc(this.collectionName + '/' + userID).update(user);
  }

  deleteUser(userId) {
    this.firestore.doc(this.collectionName + '/' + userId).delete();
  }

}
