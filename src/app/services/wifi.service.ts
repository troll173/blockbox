import { Injectable } from '@angular/core';
import { WifiWizard2 } from '@ionic-native/wifi-wizard-2/ngx';

@Injectable({
  providedIn: 'root'
})
export class WifiService {

  constructor(private wifiWizard2: WifiWizard2) { }

  findAndConnectTo(ssidRequired: string, callback) {

    this.getWifiNetworks((networks)=> {
      let found = false;
      let foundItem = null;

      networks.forEach(item => {
        console.log(item);
        if(item.SSID == ssidRequired){
         found = true;
        }
      });

      callback(found, foundItem);

    })

  }

  

  getWifiNetworks(callback){
    this.wifiWizard2.scan()
    .then(res => {
      callback(res)
    })
    .catch(error=> {
      console.error("error",error);
      callback(null);
    })
  }

 

 

 
 

}
