import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
 
import { Plugins } from '@capacitor/core';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { User } from './user.service';
const { Storage } = Plugins;

const TOKEN_KEY = 'userDetails';
const UserData_Key = 'userData';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  // Init with null to filter out the first value in a guard!
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  token = '';
  userData;
 
  constructor(
    private google: GooglePlus,
    private fb: Facebook,
    private fireAuth: AngularFireAuth,
    private platform: Platform,
  ) {
    this.loadToken();
    this.loadUserData();
  }

  async loadToken() {
    const token = await Storage.get({ key: TOKEN_KEY });    
    if (token && token.value) {
      // console.log('set token: ', token.value);
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }


  async loadUserData(){
    const userData = await Storage.get({ key: UserData_Key });    
    console.log("userData", userData);
    if (userData && userData.value) {
      this.userData = userData.value;
    } 
  }


  setUserData(user: User){
    Storage.set({key: UserData_Key, value: JSON.stringify(user)})
  }

  registerUser(value, callback){
    this.fireAuth.createUserWithEmailAndPassword(value.email, value.password)
    .then(
      res => {

        res.user.updateProfile({
          displayName: value.name,
          photoURL: "https://ionicframework.com/docs/demos/api/avatar/avatar.svg"
        }).then(function() {
          console.log("USER UPDATED!", value);
        }).catch(function(error) {
          console.log("User update Error", error);
        });

        callback(true, res);
      },
      err =>  callback(false, err))
  }


  loginWithEmail(email, password, callback){
    console.log("email and pass", email + '-- '+ password);
    this.fireAuth.signInWithEmailAndPassword(email, password)
    .then((res) => {

      console.log("service Good!", res);

      Storage.set({key: TOKEN_KEY, value: JSON.stringify(res.user)})
      this.isAuthenticated.next(true);
      callback(true, res);
    })
    .catch((err)=> {
      callback(false, err);
    })
  }



   facebookLogin(callback) {

    this.fb.login(['email'])
      .then((response: FacebookLoginResponse) => {
 
        const credential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
        this.fireAuth.signInWithCredential(credential)
          .then((response) => {
            this.isAuthenticated.next(true);
            Storage.set({key: TOKEN_KEY, value: JSON.stringify(response.user)});

            callback(true, response);

            console.log("response", response);  
          });


      }).catch((error) => {
        console.log(error);

        callback(false, error);

        alert('error:' + error);
      });
  }
 

  googleLogin(callback){
    let params: any;
    if (this.platform.is('cordova')) {
      if (this.platform.is('android')) {
        params = {
          webClientId: '136760807080-cdmhv2a6d3mascmsam6m09erbmosicsv.apps.googleusercontent.com', //  webclientID 'string'
          offline: true
        };
      } else {
        params = {};
      }
      this.google.login(params)
      .then((response) => {
        const { idToken, accessToken } = response;
     
        const credential = accessToken ? firebase.auth.GoogleAuthProvider
        .credential(idToken, accessToken) : firebase.auth.GoogleAuthProvider
            .credential(idToken);


            this.fireAuth.signInWithCredential(credential)
            .then((success) => {

            
              this.isAuthenticated.next(true);
              Storage.set({key: TOKEN_KEY, value: JSON.stringify(success.user)})
            
              callback(true, success);

              this.isAuthenticated.next(true);
              // alert('successfully');
            });
        

      }).catch((error) => {
        console.log(error);
        callback(false, error);
        alert('error:' + JSON.stringify(error));
      });
    } else{
      console.log('else...');
      this.fireAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(success => {
        console.log('success in google login', success);
       
        this.isAuthenticated.next(true);
        Storage.set({key: TOKEN_KEY, value: JSON.stringify(success.user)})

        callback(true, success);

       this.isAuthenticated.next(true);
      }).catch(err => {
        callback(false, err);
        console.log(err.message, 'error in google login');
      });
    }
  }
  

  onLoginError(err) {
    console.log(err);
  }
 
  logout(): Promise<void> {
    this.token = '';
    this.isAuthenticated.next(false);
    return Storage.remove({key: TOKEN_KEY});
  }
  
}
