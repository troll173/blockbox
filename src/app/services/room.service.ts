import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
 

export interface Room {
  name: string,  
  uid: string,
  home: string
}

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  collectionName = 'rooms';

  constructor(
    private firestore: AngularFirestore
  ) { }


  createRoom(room: Room) {
    return this.firestore.collection(this.collectionName).add(room);
  }

  readRooms() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

  searchRoomByUid(uid){
    return this.firestore.collection(this.collectionName,  ref => ref.where("uid", "==", uid)).valueChanges();
  }

  searchRoomByHome(home){
    return this.firestore.collection(this.collectionName,  ref => ref.where("home", "==", home)).valueChanges({idField: 'roomId'});
  }

  updateRoom(roomID, room: Room) {
    this.firestore.doc(this.collectionName + '/' + roomID).update(room);
  }

  deleteRoom(roomId) {
    this.firestore.doc(this.collectionName + '/' + roomId).delete();
  }

}
