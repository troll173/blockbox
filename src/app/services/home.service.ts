import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export interface Home {
  name: string,  
  creatorUid: string
}

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  collectionName = 'homes';

  constructor(
    private firestore: AngularFirestore
  ) { }


  createHome(home: Home) {
    return this.firestore.collection(this.collectionName).add(home);
  }

  readHomes() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }
  

  updateHome(homeID, home: Home) {
    this.firestore.doc(this.collectionName + '/' + homeID).update(home);
  }

  deleteHome(homeId) {
    this.firestore.doc(this.collectionName + '/' + homeId).delete();
  }

}
