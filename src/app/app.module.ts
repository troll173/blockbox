import { NgModule } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { AuthenticationService } from './services/authentication.service';

import { IonicSelectableModule } from 'ionic-selectable';
import { UserService } from './services/user.service';
import { RoomService } from './services/room.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Network } from '@ionic-native/network/ngx';
import { WifiWizard2 } from '@ionic-native/wifi-wizard-2/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { BLE } from '@ionic-native/ble/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    IonicSelectableModule,
  ],
  providers: [
    GooglePlus,
    Facebook,
    AuthenticationService,
    UserService,
    RoomService,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    Network,
    WifiWizard2,
    HTTP,
    BLE,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy } 
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
