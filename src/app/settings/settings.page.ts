import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  private token = {};

  constructor(private authService: AuthenticationService, private navCtrl: NavController, ) { 
   
    this.authService.loadToken().then(value => {
      this.token = JSON.parse(this.authService.token);
      console.log("token", this.token);
    })

    this.authService.loadUserData().then(value => {
      console.log("userData", this.authService.userData);
    })
    
  }

  ngOnInit() {
  
  }

  openAccounts(){
    this.navCtrl.navigateForward("tabs/settings/accounts")
  }


  logout(){
    this.authService.logout();
    this.navCtrl.navigateRoot("/auth/login");
  }
}
