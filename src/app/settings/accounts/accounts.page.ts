import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.page.html',
  styleUrls: ['./accounts.page.scss'],
})
export class AccountsPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  configSpotify(){
    this.navCtrl.navigateForward("tabs/settings/accounts/spotify")
  }

}
