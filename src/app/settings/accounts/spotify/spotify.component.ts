import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Console } from 'node:console';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { User, UserService } from 'src/app/services/user.service';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-spotify',
  templateUrl: './spotify.component.html',
  styleUrls: ['./spotify.component.scss'],
})
export class SpotifyComponent implements OnInit {

  private clientId = environment.spotify.client_id;
  private redirectUrl = environment.spotify.redirect_url;

  private spotifyData = null;
  private userData;
 
  constructor(private iab: InAppBrowser, private authService: AuthenticationService, private userService: UserService, private spotifyService: SpotifyService) { }

  ngOnInit() {
    this.initUserData();

  }


  initUserData(){
    this.authService.loadUserData().then(value => {
      let parsedData = JSON.parse(this.authService.userData);
    

      this.userService.searchUserByUid(parsedData.uid)
      .subscribe((res: any[])  => {
        
        this.userData = res[0];

        if(this.userData.spotify != ''){
          let spotify = JSON.parse(this.userData.spotify);
          this.spotifyService.getUserInfo(spotify.access_token, (success, data) => {
            if(success){
              this.spotifyData = JSON.parse(data);
              console.log(this.spotifyData);
              console.log("img",this.spotifyData.images[0].url)
            }
          });
        }
       
       
      })

    })
  }

  connectSpotify(){

    let scope = "user-read-private%20user-read-email%20user-read-playback-state%20user-read-playback-position";

    let url = 'https://accounts.spotify.com/authorize?client_id='+this.clientId+'&response_type=code&redirect_uri='+encodeURIComponent(this.redirectUrl)+"&scope="+scope+"&show_dialog=true";
    
    const browser = this.iab.create(url);
  
    browser.on('loadstop').subscribe(event => {
    
     if(event.url.includes(this.redirectUrl)){

      let urlArr = event.url.split("?code=");
 
        console.log("CODE", urlArr[1])
        
        this.spotifyService.requestTokens(urlArr[1], (success, data)=>{
          if(success){
            this.saveSpotifyCode(data);
          }
         
        })
        browser.close();

     
     }

   });
  }


  saveSpotifyCode(code){

    let newUser = this.userData;
    let userDataId = newUser.userDataId; 
    delete newUser.userDataId; 

    let user:User = newUser;  
    user.spotify = code;


    this.userService.updateUser(userDataId, user)
    .then(res => {

      console.log("resss", user);

      this.authService.setUserData(user);
      this.initUserData();

    })
    .catch(err => {
      console.error("update Failed", err);
    })

    
  }
}
