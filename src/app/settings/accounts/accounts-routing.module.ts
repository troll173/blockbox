import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountsPage } from './accounts.page';
import { SpotifyComponent } from './spotify/spotify.component';

const routes: Routes = [
  {
    path: '',
    component: AccountsPage
  },
  {
    path: 'spotify',
    component: SpotifyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsPageRoutingModule {}
