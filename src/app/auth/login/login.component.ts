import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Home, HomeService } from 'src/app/services/home.service';
import { User, UserService } from 'src/app/services/user.service';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

const TOKEN_KEY = 'userData';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController, private homeService: HomeService,
    private navCtrl: NavController, private userService: UserService,
    private loadingController: LoadingController
    ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });


  }

  async loginEmail(value){
    const loading = await this.loadingController.create();
    await loading.present();

    console.log("credentials",value);
    
    this.authService.loginWithEmail(value.email, value.password, async (success, res) => {
      if(success){

        await loading.dismiss();        
        this.navCtrl.navigateRoot('/tabs/home', { replaceUrl: true });

      }else{

        await loading.dismiss();
        console.log(res);
        const alert = await this.alertController.create({
          header: 'Login failed',
          message: res.message,
          buttons: ['OK'],
        });
  
        await alert.present();

      }
    })
    
     
  }

  googleLogin(){
    this.authService.googleLogin(async (success, res)=>{
      if(success){

        console.log("checkIfUserExists");
        this.checkIfUserExists(res.user);
     
        this.navCtrl.navigateRoot('/tabs/home', { replaceUrl: true });

      }else{

        console.log(res);
        const alert = await this.alertController.create({
          header: 'Login failed',
          message: res.message,
          buttons: ['OK'],
        });
  
        await alert.present();

      }
    });
  }

  facebookLogin(){
    this.authService.facebookLogin(async (success, res)=> {
      if(success){

        this.checkIfUserExists(res.user);
        
        this.navCtrl.navigateRoot('/tabs/home', { replaceUrl: true });

      }else{

        console.log(res);
        const alert = await this.alertController.create({
          header: 'Login failed',
          message: res.message,
          buttons: ['OK'],
        });
  
        await alert.present();

      }
    });
  }


  checkIfUserExists(user){
    this.userService.searchUserByUid(user.uid)
    .subscribe((users : any) => {
      
      console.log("searched "+user.uid, users);

      if(users.length < 1){

        this.createHome(user.displayName + " Home", user.uid, (success, home)=> {
          if(success){
          
            this.createUser(user.displayName, user.uid, home.id, (success, created)=> {
              console.log("User Created", created);
            })

          }
        })
          

      }else{

        console.log("for UserData", users[0]);

        let oldUserData = users[0];
        delete oldUserData.userDataId; 
        let userData: User = oldUserData;

        this.authService.setUserData(userData);

      }
    
    })
  }

  createUser(name, uid, homeId, callback){
    let user:User = {
      name: name,
      uid: uid,
      home: homeId,
      spotify: ''
    };
  
    this.userService.createUser(user)
    .then(res => {


      this.userService.findUser(res.id)
      .subscribe(res => {
        console.log("Created user info", res);

        Storage.set({key: TOKEN_KEY, value: JSON.stringify(res)});

      });

      callback(true, res);
    })
    .catch(err => {
      callback(false, err);
    })
  }


  createHome(name, uid, callback){

    let home:Home = {
      name: name,
      creatorUid: uid
    }

    this.homeService.createHome(home)
    .then(res => {
      console.log("Home creation", res.id);
      callback(true, res);
    })
    .catch(err => {
      console.log("Home creation Error", err);
      callback(false, err);
    })
  }

  openMain(){
    this.navCtrl.navigateRoot('/tabs/home', { replaceUrl: true });
  }

  toSignup(){
    this.navCtrl.navigateForward("/auth/signup");
  }
}
