import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Plugins } from '@capacitor/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Home, HomeService } from 'src/app/services/home.service';
import { User, UserService} from 'src/app/services/user.service';
const { Storage } = Plugins;

const TOKEN_KEY = 'userData';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
 
  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' },
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  
 
  constructor(private navCtrl: NavController, private authService: AuthenticationService, 
    private formBuilder: FormBuilder,  private toastController: ToastController, private homeService: HomeService,
    private userService: UserService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }


  async signUp(value) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService.registerUser(value, async (success, res)=>{
      if(success){
        console.log("REG",res);
     

        this.createHome(value.name + " Home",res.user.uid, (success, home)=> {
          if(success){
 
            this.createUser(value.name, res.user.uid, home.id, (success, created)=> {
              console.log("User Created", created);
            })

          }
        })



        await loading.dismiss();   
        this.errorMessage = "";
        this.successMessage = "Your account has been created. Please log in.";
        this.navCtrl.navigateRoot('/auth/login');
      }else{
        console.log("FAILED",res);
        this.errorMessage = res.message;
        this.successMessage = "";
 
        const toast =  await this.toastController.create({
          header: 'Signup Failed',
          message: this.errorMessage,
          duration: 2000,
          position: 'top',
          color: 'danger'
        });

        await loading.dismiss();   
        toast.present();
 
      }
    });
     
  }

  createUser(name, uid, homeId, callback){
    let user:User = {
      name: name,
      uid: uid,
      home: homeId,
      spotify: ''
    };
  
    this.userService.createUser(user)
    .then(res => {

      this.userService.findUser(res.id)
      .subscribe(res => {
        console.log("Created user info", res);

        Storage.set({key: TOKEN_KEY, value: JSON.stringify(res)});

      });

      callback(true, res);
    })
    .catch(err => {
      callback(false, err);
    })
  }


  createHome(name, uid, callback){

    let home:Home = {
      name: name,
      creatorUid: uid
    }

    this.homeService.createHome(home)
    .then(res => {
      console.log("Home creation", res.id);
      callback(true, res);
    })
    .catch(err => {
      console.log("Home creation Error", err);
      callback(false, err);
    })
  }
  

  toLogin(){
    this.navCtrl.navigateBack("/auth/login");
  }

}
