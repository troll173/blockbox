import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AutomationPage } from './automation.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { AutomationPageRoutingModule } from './automation-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    AutomationPageRoutingModule
  ],
  declarations: [AutomationPage]
})
export class AutomationPageModule {}
