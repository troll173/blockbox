import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { IonicSelectableComponent } from 'ionic-selectable';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Room, RoomService } from 'src/app/services/room.service';
import { WifiService } from 'src/app/services/wifi.service';
import { BLE } from '@ionic-native/ble/ngx'; 
import { ConnectWifiComponent } from 'src/app/components/connect-wifi/connect-wifi.component';


class Device {
  public id: number;
  public name: string;
  public slug: string;
  public description: string;
  public method: string;
  public connectionInfo: string;
}

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.scss'],
})
export class AddDeviceComponent implements OnInit {
 

  auto = 0;
  scanning = false;
  room = '';
  rooms;

  devices: Device[];
  device: Device;

  private userData;
  
  constructor(private alertController: AlertController, private roomService: RoomService, private authService: AuthenticationService, private alertCtrl: AlertController,
    private modalController: ModalController, private wifiService: WifiService, private ble: BLE, private loadingController: LoadingController) { 
    this.devices = [
      { id: 0, name: 'Philips Hue', slug: 'phue', description: 'Philips Hue', method: 'wifi', connectionInfo: '' },
      { id: 1, name: 'Reflect Mirror', slug: 'reflect', description: 'Reflect Smart Mirror', method: 'ble', connectionInfo: '{"ble":"Reflect","service":"ff51b30e-d7e2-4d93-8842-a7c4a57dfb07","characteristic":"ff51b30e-d7e2-4d93-8842-a7c4a57dfb1b","homeCharacteristic":"48d51a79-4ee8-41b9-8994-f58a78f6e7d6"}' },
    ];


    this.authService.loadUserData().then(value => {
      console.log("this.authService.userData", this.authService.userData);
      this.userData = JSON.parse(this.authService.userData);
      console.log("userdata", this.userData);

      this.getRooms();
    })

  }

  ngOnInit() { }

  getRooms(){
    console.log("Searching in home ",this.userData.home);
    this.roomService.searchRoomByHome(this.userData.home)
    .subscribe(res => {
      console.log("new Rooms",res);
      this.rooms = res;
    },error => {
      console.log("getRooms Error",error);
    })
  }

  switchType(event){
    this.auto = event.detail.value;
  }

  deviceChange(event: {
    component: IonicSelectableComponent,
    value: any
  }){
    console.log('value:', event.value);
  }

   
  async presentRoomNameAlert(e) {
   
    if(e.detail.value == "+=add"){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Room Name',
        inputs: [
          {
            name: 'room',
            type: 'text',
            placeholder: 'Enter Room Name'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              this.room = '';
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Ok',
            handler: (value) => {
              if(value.room != ""){
                
                this.createRoom(value.room, this.userData.home ,this.userData.uid, (success, res)=> {
                    if(success){
                      console.log("Success", res.id);
                      this.room = res.id;
                    }else{
                      console.error(res);
                    }
                });

                console.log('Confirm Ok', value.room);
              }else{
                this.room = '';
                console.log('Confirm Ok', "No Value");
              }
            }
          }
        ]
      });
  
      await alert.present();
    }

   
  }



  createRoom(roomName, home, creator, callback){
    let room: Room = {
      name: roomName,  
      uid: creator,
      home: home
    }

    this.roomService.createRoom(room)
    .then(res => {
      callback(true, res);
    })
    .catch(err => {
      callback(false, err);
    })

  }


  async scanForDevice(){

    if(this.room == ''){
      const alert = await this.alertController.create({
        header: 'Select Room First!',
        subHeader: 'Please select a room for this device.'
      });
      await alert.present();
    }else{

        let connectInfo = JSON.parse(this.device.connectionInfo);
        
        const loading = await this.loadingController.create({
          message: 'Scanning...',
          spinner: "bubbles",
        });

        const alert = await this.alertController.create({
          header: 'Scan Failed',
          subHeader: 'Device not found!'
        });

        console.log(connectInfo);

        switch(this.device.method){
          case "wifi":
            this.wifiService.findAndConnectTo(connectInfo.ssid, (found, network)=> {
              console.log('found', found);
              console.log("network", network);
            });
          break;

          case "ble":
            console.log("checking ble", connectInfo);

            let dfound = false;
            
            await loading.present();

            this.ble.startScan([])
            .subscribe(found => {
              console.log("ble device",found);
              dfound = true;
              if(found.name == connectInfo.ble){
                this.ble.stopScan()
                .then(async val => {
                  console.log("Stoppped Scan", val);
                  await loading.dismiss();        


                  this.selectWifiNetwork(wifiData => {
                    this.connectToBLE(found, connectInfo, wifiData);
                  });
                  
                  
                })
              
              }

    
              
              setTimeout(() => {
                this.ble.stopScan().then(async () => {
                  await loading.dismiss();        
                  console.log('scan stopped'); 
                  
                  if(!dfound){
                    await loading.dismiss();        
                    await alert.present();
                  }
                  
                  });
              }, 10000);

            })
    

          break;
        }
    
    }
 
  }


  async selectWifiNetwork(callback) {
    const modal = await this.modalController.create({
      component: ConnectWifiComponent,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    callback(data);
  }



  async connectToBLE(device, connectInfo, wifiDetails) {
     console.log("connecting to " + device.name, wifiDetails);
  

     const loading = await this.loadingController.create({
      message: 'Connecting...',
      spinner: "bubbles",
    });

    
    const alert = await this.alertController.create({
      header: 'Connection Failed',
      subHeader: 'Failed to connect device!'
    });


    await loading.present();
     this.ble.autoConnect(device.id, 
      connected => {
        console.log("connected",connected);


        this.ble.write(device.id, connectInfo.service, connectInfo.characteristic, this.stringToBytes(wifiDetails.SSID+":"+wifiDetails.password))
        .then(res => {
          console.log("set wifi", res);

          let parent = this;

          console.log('waiting before confirming!'); 
          setTimeout(() => {
            parent.confirmConnection(device, connectInfo, loading, alert);
          }, 15000);

         
       
        })
        .catch(err => {
          console.log("ble.write Error", err);
          this.ble.disconnect(device.id)
          .then(async res => {
            console.log("Disconnected after ble.write Error", res);
            await loading.dismiss();
   
          });
        })

        // setTimeout(() => {
        //   this.ble.disconnect(device.id)
        //   .then(async res => {
        //     console.log("Disconnected after timeout", res);
        //     await loading.dismiss();
   
        //   });
        // }, 40000);
       

     },
     async disconnected=> {
      console.log("disconnected",disconnected);
      await loading.dismiss();
      await alert.present();
     });
 
  }


 async confirmConnection(device, connectInfo, loading, alert){
  
  

  this.ble.read(device.id, connectInfo.service, connectInfo.characteristic)
  .then(async read=> {
    
    console.log("read data", read);

    this.setReflectConfig(device, connectInfo, loading, alert);


  })
  .catch(err => {
    console.error("read error", err);

    this.ble.disconnect(device.id)
    .then(async res => {
      console.log("Disconnected after e", res);
      await loading.dismiss(); 
    });
  })


 }
 
 
 async setReflectConfig(device, connectInfo, loading, alert){

  const connectedAlert = await this.alertController.create({
    header: 'Connection Success',
    subHeader: 'Device sucessfully connected!'
  });

  
  loading.message = "Setting up Device...";
  this.ble.write(device.id, connectInfo.service, connectInfo.homeCharacteristic, this.stringToBytes(this.userData.home))
        .then(res => {

          console.log("set reflect config", res);

          setTimeout(() => {
            this.ble.read(device.id, connectInfo.service, connectInfo.homeCharacteristic)
            .then(async read=> {
              
              console.log("read config", read);
              

              this.ble.disconnect(device.id)
              .then(async res => {
                console.log("Disconnected after connection", res);
                await loading.dismiss();

                if(this.bytesToString(read) == "1"){
                  await connectedAlert.present();
                }else{
                  await alert.present();
                }

              });
              
            });
          }, 15000);
          
        })
 }

 


stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
        array[i] = string.charCodeAt(i);
     }
     return array.buffer;
 }

bytesToString(buffer) {
  return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

}



