import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { HomePageRoutingModule } from './home-routing.module';
import { IonicSelectableModule } from 'ionic-selectable';
import { AddDeviceComponent } from './add-device/add-device.component';
import { ScanningComponent } from '../components/scanning/scanning.component';
import { PermissionsComponent } from '../components/permissions/permissions.component';
import { ComponentsModule } from '../components/components.module';
 
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    HomePageRoutingModule,
    IonicSelectableModule,
    ComponentsModule
  ],
  declarations: [HomePage, AddDeviceComponent]
})
export class HomePageModule {}
